# launch

```bash
git clone https://nawada@bitbucket.org/nawada/discord-bot.git
cd discord-bot
bundle install --path vendor/bundle
# edit bot.rb
bundle exec ruby bot.rb
```

## about bot.rb

To use this ruby script, you must edit the `bot.rb` file.

First of all, [get the discord bot token](https://discordapp.com/developers/applications/me).

When you get a token, set token at line 24.

Also you need to set your Client ID.

Then you can launch the bot!