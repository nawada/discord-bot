# coding: utf-8

require 'pp'
require 'discordrb'

CONTROLL_MAP_NAMES = [
    'NEPAL',
    'ILIOS',
    'LIJIANG TOWER',
]

MAP_NAMES = [
    # 'ECOPOINT: ANTARCTICA',
    'HANAMURA',
    'TEMPLE OF ANUBIS',
    'VOLSKAYA INDUSTRIES',
    'DORADO',
    'WATCHPOINT:GIBRALTAR',
    'ROUTE66',
    "KING'S ROW",
    'NUMBANI',
    'HOLLYWOOD',
    'EICHENWALDE',
    'OASIS'
] + CONTROLL_MAP_NAMES

bot = Discordrb::Bot.new token: '', client_id: ''

bot.mention do |event|
  message = event.message.author.mention + ' '

  if event.message.content.index(/(たい|ぶれーく|タイ|ブレーク|tie)/)
    message += CONTROLL_MAP_NAMES.sample
  else
    message += MAP_NAMES.sample
  end

  event.message.reply(message)
end

bot.run

